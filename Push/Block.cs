﻿using UnityEngine;
using System.Collections;

public class Block : MonoBehaviour {

	public Transform player;
	private RigidbodyConstraints2D rb2d;
	private int change = 0;
	private int stop = 0;
	public Transform location;
	public GameObject[] prefab;
	public GameObject[] prefab1;
	private int fab;
	private float timeLeft = 8;
	private bool one = false;
	private bool one1 = false;
	private bool time;
	private int ran;
	private int ran1;
	private bool grounded = false;


	void spawn (){

		fab = Random.Range (0, 4);


			prefab1 [fab] = Instantiate (prefab [fab], location.transform.position, Quaternion.Euler (0, 0, 0)) as GameObject;
		
		}
		
		
	//Turning off rigidbody on collision
	void OnCollisionEnter2D(Collision2D coll) {

		if (coll.gameObject.tag == "Player") {

			Application.LoadLevel ("EndGame");
		}

		if (coll.gameObject.tag == "Respawn") {

			grounded = true;
		}


		if (coll.gameObject.tag == "Finish") {

			grounded = true;
		}





	}


		
	void Update () {

		timeLeft -= Time.deltaTime;


		if (timeLeft <= 0 && one == false && grounded == true) {
			grounded = false;
			spawn ();
			one = true;
			stop++;


		}

		//Rotation
		if (Input.GetKeyDown(KeyCode.A) && stop ==0)
		{
			change = change + 90;

			transform.rotation = Quaternion.Euler(0, 0, change);

		}
		if (Input.GetKeyDown(KeyCode.D)&& stop ==0)
		{
			change = change - 90;

			transform.rotation = Quaternion.Euler(0, 0, change);
		}
			
		if (change == 360 || change == -360) {

			change = 0;
		}

		//Block movement

		if (change == 0 && stop ==0)
		{
			transform.Translate (Vector2.down * .5f * Time.deltaTime);
		}

		if ((change == 90 || change == -270) && stop ==0)
		{
			transform.Translate (Vector2.left * .5f * Time.deltaTime);
		}

		if (change == 180 || change == -180 && stop ==0)
		{
			transform.Translate (Vector2.up * .5f * Time.deltaTime);

		}

		if (change == 270 || change == -90 && stop ==0)
		{
			transform.Translate (Vector2.right * .5f * Time.deltaTime);
		}
			
		if (Input.GetKeyDown(KeyCode.RightArrow) && change == 0 && stop ==0)
		{
			transform.Translate (Vector2.right * 9f * Time.deltaTime);

		}
		if (Input.GetKeyDown(KeyCode.RightArrow) && (change == 90 || change == -270) && stop ==0)
		{
			transform.Translate (Vector2.down * 9f * Time.deltaTime);
		}
			
		if (Input.GetKeyDown(KeyCode.RightArrow) && (change == 180 || change == -180)&& stop ==0)
		{
			transform.Translate (Vector2.left * 9f * Time.deltaTime);
		}

		if (Input.GetKeyDown(KeyCode.RightArrow) && (change == 270 || change == -90)&& stop ==0)
		{
			transform.Translate (Vector2.up * 9f * Time.deltaTime);
		}
			
		if (Input.GetKeyDown(KeyCode.LeftArrow) && change == 0&& stop ==0)
		{
			transform.Translate (Vector2.left * 9f * Time.deltaTime);
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow) && (change == 90 || change == -270)&& stop ==0)
		{
			transform.Translate (Vector2.up * 9f * Time.deltaTime);
		}
			
		if (Input.GetKeyDown(KeyCode.LeftArrow) && (change == 180 || change == -180)&& stop ==0)
		{
			transform.Translate (Vector2.right * 9f * Time.deltaTime);
		}

		if (Input.GetKeyDown(KeyCode.LeftArrow) && (change == 270 || change == -90)&& stop ==0)
		{
			transform.Translate (Vector2.down * 9f * Time.deltaTime);
		}
			
	}
}